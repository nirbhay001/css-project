## In this project, I have made a Payment user-interface project by using:
* HTML
* CSS
## Project Description
* In this project, I have made a Payment user interface using HTML and CSS with a beautiful layout and user interface.
* I have used CSS properties like flex and grid etc.
## Visit this site to see the user-interface
* https://exquisite-eclair-18151a.netlify.app/
